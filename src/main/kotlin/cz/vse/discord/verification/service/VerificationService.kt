package cz.vse.discord.verification.service

import com.mailgun.api.v3.MailgunMessagesApi
import com.mailgun.model.message.Message
import cz.vse.discord.verification.configuration.properties.MailgunConfigurationProperties
import cz.vse.discord.verification.domain.PendingVerification
import cz.vse.discord.verification.repository.PendingVerificationRepository
import org.springframework.stereotype.Service
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import java.nio.ByteBuffer
import java.security.SecureRandom
import kotlin.random.Random

@Service
class VerificationService(
    private val repository: PendingVerificationRepository,
    private val templates: TemplateEngine,
    private val mailgun: MailgunMessagesApi,
    private val mailgunConfiguration: MailgunConfigurationProperties
) {

    suspend fun createVerification(username: String): PendingVerification {
        val email = "$username@vse.cz"
        val code = generateVerificationCode()

        val verification = PendingVerification(
            username = username,
            code = code
        )

        repository.deleteAllByUsername(username)
        repository.save(verification)

        sendEmailWithCode(email, code)

        return verification
    }

    suspend fun completeVerification(code: String): String? {
        val verification = repository.findByCode(code) ?: return null
        val username = verification.username

        repository.delete(verification)

        return username
    }

    @Suppress("UsePropertyAccessSyntax")
    private fun sendEmailWithCode(email: String, code: String) {
        val template = "email/code"
        val context = Context().apply {
            setVariable("email", email)
            setVariable("code", code)
        }

        val html = templates.process(template, context)
        val message = Message.builder()
            .from(mailgunConfiguration.sender)
            .to(email)
            .subject("Verifikace školního účtu VŠE")
            .html(html)
            .build()

        mailgun.sendMessage(mailgunConfiguration.domain, message)
    }

    private fun generateVerificationCode(): String {
        val seed = ByteBuffer.wrap(SecureRandom.getSeed(Long.SIZE_BYTES)).long
        val random = Random(seed)
        val charset = ('0'..'9') + ('a'..'z') + ('A'..'Z')

        return generateSequence { charset.random(random) }
            .take(32)
            .joinToString("")
    }
}