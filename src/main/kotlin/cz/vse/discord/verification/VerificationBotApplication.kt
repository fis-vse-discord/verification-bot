package cz.vse.discord.verification

import cz.vse.discord.verification.configuration.properties.DiscordConfigurationProperties
import cz.vse.discord.verification.configuration.properties.MailgunConfigurationProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(
    DiscordConfigurationProperties::class,
    MailgunConfigurationProperties::class
)
class VerificationBotApplication

fun main(args: Array<String>) {
    runApplication<VerificationBotApplication>(*args)
}
