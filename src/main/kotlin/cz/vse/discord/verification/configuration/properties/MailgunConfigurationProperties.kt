package cz.vse.discord.verification.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "mailgun")
data class MailgunConfigurationProperties(
    val sender: String,
    val domain: String,
    val apiKey: String,
)