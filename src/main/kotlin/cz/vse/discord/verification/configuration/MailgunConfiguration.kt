package cz.vse.discord.verification.configuration

import com.mailgun.api.v3.MailgunMessagesApi
import com.mailgun.client.MailgunClient
import cz.vse.discord.verification.configuration.properties.MailgunConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MailgunConfiguration {

    @Bean
    fun mailgunMessagesApi(configuration: MailgunConfigurationProperties): MailgunMessagesApi {
        return MailgunClient
            .config("https://api.eu.mailgun.net/", configuration.apiKey)
            .createApi(MailgunMessagesApi::class.java)
    }
}