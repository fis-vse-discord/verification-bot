package cz.vse.discord.verification.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "discord")
data class DiscordConfigurationProperties(
    val token: String,
    val guild: Long,
    val confirmationChannel: Long,
    val verifiedRole: Long
)